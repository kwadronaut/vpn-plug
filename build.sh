#!/bin/sh
#
# Install script for openvpn inside a Docker container.
# we install shapeshifter-dispatcher too
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="ca-certificates git"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

install_packages ${BUILD_PACKAGES}

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

# Install openvpn go.
install_packages openvpn iptables golang\
    || die "could not install packages"

# In buster, default is nftables
update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

mkdir -p /srv/leap/go
export GOPATH=/srv/leap/go
go get -u -v 0xacab.org/leap/shapeshifter-dispatcher/shapeshifter-dispatcher

# Clean up.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
