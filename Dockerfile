FROM bitnami/minideb:buster

LABEL maintainer="kwadronaut <kwadronaut@leap.se>"

COPY build.sh /tmp/build.sh
RUN /tmp/build.sh && rm /tmp/build.sh

# iptables IP is identical as openvpn config
ENTRYPOINT ["/bin/sh", "-c", "iptables -t nat -A POSTROUTING -s 10.41.0.0/24 -j MASQUERADE", "/srv/leap/go/bin/shapeshifter-dispatcher -transparent -server -state ${STATE:/srv/leap/shapeshifter-state} -orport ${RHOST:127.0.0.1}:${RPORT:1194} -transports obfs4 -bindaddr obfs4-${IP_PUBLIC}:${OBFSPORT:23042} -logLevel ${LOGLEVEL:WARN} -enableLogging -extorport 127.0.0.1: ${EXTORPORT:3334}"]

# Internally uses port 443/tcp
EXPOSE 443/tcp

WORKDIR /srv/leap/shapeshifter-state

