docker-openvpn-PT
===

A simple openvpn and shapeshifter-dispatcher instance in a Docker container. To
ease networking setup, shapeshifter passes traffic on to the VPN inside the
container. Configuration is external and must be supplied by mounting
/etc/openvpn, and a statedirectory for shapeshifter
/srv/leap/shapeshifter-state .
